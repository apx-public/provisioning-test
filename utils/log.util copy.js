"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogUtil = void 0;
var winston = __importStar(require("winston"));
var config = require('config');
var morgan = require('morgan');
var LogUtil = /** @class */ (function () {
    function LogUtil() {
    }
    LogUtil.date = function () {
        return (new Date()).toISOString().substring(0, 19);
    };
    LogUtil.createLogger = function () {
        var options = config.log.winston;
        options.transports = options.transports || [];
        options.transports.push(new winston.transports.Console());
        if (config.log.format == "cli") {
            options.format = winston.format.combine(winston.format.cli());
            winston.addColors({
                http: 'yellow'
            });
        }
        return winston.createLogger(options);
    };
    LogUtil.logger = LogUtil.createLogger();
    LogUtil.stream = {
        write: function (message, encoding) {
            LogUtil.logger.http(message.trim());
        }
    };
    LogUtil.morgan = morgan(config.log.morgan, { stream: LogUtil.stream });
    return LogUtil;
}());
exports.LogUtil = LogUtil;
//# sourceMappingURL=log.util.js.map