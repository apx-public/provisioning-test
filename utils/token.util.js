"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenUtil = void 0;
var jwt = require('jsonwebtoken');
var config = require('config');
var TokenUtil = /** @class */ (function () {
    function TokenUtil() {
    }
    TokenUtil.createToken = function (payload) {
        return TokenUtil.createTokenWithKey(payload, config.sessionKey, "30d");
    };
    TokenUtil.validateToken = function (token) {
        return TokenUtil.validateTokenWithKey(token, config.sessionKey);
    };
    TokenUtil.createTokenWithKey = function (payload, sessionKey, expireIn) {
        console.log('createToken -> ', payload);
        return jwt.sign(payload, sessionKey, { expiresIn: expireIn });
    };
    TokenUtil.validateTokenWithKey = function (token, sessionKey) {
        try {
            return jwt.verify(token, sessionKey);
        }
        catch (err) {
            return false;
        }
    };
    return TokenUtil;
}());
exports.TokenUtil = TokenUtil;
//# sourceMappingURL=token.util.js.map