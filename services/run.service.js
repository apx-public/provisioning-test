"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RunService = void 0;
var log_util_1 = require("../utils/log.util");
var run_util_1 = require("./run.util");
var vm = require('vm');
var RunService = /** @class */ (function () {
    function RunService() {
    }
    RunService.prototype.run = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var context, output, log, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(data);
                        log_util_1.LogUtil.logger.info("run " + data.script_name + " on " + data.device_code);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        context = {
                            params: data.params || {},
                            device: data.ufields,
                            serial_number: data.serial_number
                        };
                        log_util_1.LogUtil.logger.debug("run " + data.script_name + " on " + data.device_code + " IN " + JSON.stringify(context));
                        //context.conn = conn;
                        context.util = new run_util_1.RunUtil(context, data.connection);
                        return [4 /*yield*/, RunService.evalInContext(data.script, context)];
                    case 2:
                        _a.sent();
                        output = '';
                        if (context.util.conn) {
                            output = context.util.conn.output_log;
                        }
                        log = context.util.l_log.join("\n");
                        //delete(context.conn);
                        context.util.release();
                        delete (context.util);
                        log_util_1.LogUtil.logger.debug("run " + data.script_name + " on " + data.device_code + " OUT " + JSON.stringify(context));
                        return [2 /*return*/, {
                                ufields: context.device,
                                params: context.params,
                                output: output,
                                log: log
                            }];
                    case 3:
                        err_1 = _a.sent();
                        log_util_1.LogUtil.logger.error("Error running " + data.script_name + " on " + data.device_code + " -> " + err_1);
                        return [2 /*return*/, {
                                error: err_1 + " "
                            }];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    RunService.evalInContext = function (js, context) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var sandbox = context;
                        sandbox._resolve = resolve;
                        sandbox._reject = reject;
                        /*
                        sandbox._reject = (error:any)=>{
                            console.log("REJECT!!!", error);
                            reject(error);
                        }
                        */
                        vm.createContext(sandbox);
                        /*
                                    js = `(async () => {
                                            try {
                                                ${js};
                                                _resolve();
                                            } catch (error) {
                                                _reject(error);
                                            }
                                          })();`
                        */
                        try {
                            js = "(async () => { \n                try {\n                    ".concat(js, "; \n                    _resolve(); \n                } catch (error) {\n                    _reject(error);\n                }\n            })();");
                            vm.runInContext(js, sandbox, { breakOnSigint: true });
                        }
                        catch (error) {
                            console.log("catch!!!", error);
                            reject(error);
                        }
                    })];
            });
        });
    };
    return RunService;
}());
exports.RunService = RunService;
//# sourceMappingURL=run.service.js.map