"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SshUtil = void 0;
var config = require('config');
var readFileSync = require('fs').readFileSync;
var readline = require('readline');
var Client = require('ssh2').Client;
var SshUtil = /** @class */ (function () {
    function SshUtil() {
        this.conn = new Client({ keepaliveInterval: 5000 });
        this.output = "";
    }
    SshUtil.prototype.connect = function (connInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        try {
                            _this.conn.on('ready', function () {
                                _this.conn.shell(function (err, stream) {
                                    if (err)
                                        reject(err);
                                    _this.stream = stream;
                                    stream.on('close', function () {
                                        _this.conn.end();
                                    }).on('data', function (data) {
                                        _this.output += data.toString();
                                    }).stderr.on('data', function (data) {
                                        _this.output += data.toString();
                                    });
                                    resolve(true);
                                });
                            }).connect({
                                host: connInfo.host,
                                port: connInfo.port || 22,
                                username: connInfo.username,
                                password: connInfo.password
                            });
                        }
                        catch (error) {
                            reject("Error connecting to " + connInfo.host + ": " + error);
                        }
                    })];
            });
        });
    };
    SshUtil.prototype.send = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.stream.write(data + "\n");
                return [2 /*return*/];
            });
        });
    };
    SshUtil.prototype.find = function (prev, post, timeout) {
        var _this = this;
        var _find = function (prev, post, timeout, resolve, reject) {
            var text = _this.output;
            try {
                for (var _i = 0, prev_1 = prev; _i < prev_1.length; _i++) {
                    var p = prev_1[_i];
                    text = text.split(p)[1];
                }
                text = text.split(post)[0];
                resolve(text);
            }
            catch (error) {
                if (timeout > 0) {
                    setTimeout(function () {
                        _find(prev, post, timeout - 500, resolve, reject);
                    }, 500);
                }
                else {
                    reject("find Timeout: " + JSON.stringify(prev));
                }
            }
        };
        return new Promise(function (resolve, reject) {
            _find(prev, post, timeout, resolve, reject);
        });
    };
    return SshUtil;
}());
exports.SshUtil = SshUtil;
//# sourceMappingURL=ssh.util.js.map