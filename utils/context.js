"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Context = void 0;
var log_util_1 = require("../utils/log.util");
var pg_util_1 = require("../utils/pg.util");
var contextOptions_1 = require("./contextOptions");
var Context = /** @class */ (function () {
    function Context() {
    }
    Context.getDefaultContextOptions = function () {
        var options = new contextOptions_1.ContextOptions();
        options.resource = '';
        options.uid = '';
        options.default_schema = 'public';
        return options;
    };
    Context.createContext = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var context_id, context, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        options = options || this.getDefaultContextOptions();
                        context_id = options.uid + '-' + Math.floor(Math.random() * 1000000);
                        log_util_1.LogUtil.logger.debug('createContext: ' + context_id + ' ' + JSON.stringify(options));
                        context = new Context();
                        context.options = options;
                        context.context_id = context_id;
                        context.uid = options.uid;
                        context.roles = options.roles;
                        _a = context;
                        return [4 /*yield*/, pg_util_1.PgUtil.getClient(options.default_schema || 'public')];
                    case 1:
                        _a.db = _b.sent();
                        //await PgUtil.setDefaultSchema(context, options.default_schema || 'public');
                        return [2 /*return*/, context];
                }
            });
        });
    };
    Context.releaseContext = function (context) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                log_util_1.LogUtil.logger.debug('releaseContext: ' + context.context_id);
                context.db.release();
                return [2 /*return*/];
            });
        });
    };
    Context.prototype.queryFirst = function (sql, params) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                log_util_1.LogUtil.logger.verbose(sql + ' ' + JSON.stringify(params));
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var result, error_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    return [4 /*yield*/, this.db.query(sql, params)];
                                case 1:
                                    result = _a.sent();
                                    if (result.rows && result.rows.length > 0)
                                        resolve(result.rows[0]);
                                    else
                                        resolve({});
                                    return [3 /*break*/, 3];
                                case 2:
                                    error_1 = _a.sent();
                                    reject(error_1);
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    Context.prototype.query = function (sql, params) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                log_util_1.LogUtil.logger.verbose(sql + ' ' + JSON.stringify(params));
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var result, error_2;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    return [4 /*yield*/, this.db.query(sql, params)];
                                case 1:
                                    result = _a.sent();
                                    resolve(result.rows);
                                    return [3 /*break*/, 3];
                                case 2:
                                    error_2 = _a.sent();
                                    log_util_1.LogUtil.logger.error(error_2 + ' executing ' + sql + ' ' + JSON.stringify(params));
                                    reject(error_2);
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    Context.prototype.beginTransaction = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.query('BEGIN', [])];
            });
        });
    };
    Context.prototype.commitTransaction = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.query('COMMIT', [])];
            });
        });
    };
    Context.prototype.rollbackTransaction = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.query('ROLLBACK', [])];
            });
        });
    };
    return Context;
}());
exports.Context = Context;
//# sourceMappingURL=context.js.map