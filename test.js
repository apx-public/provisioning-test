"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

var run_util_1 = require("./services/run.util");

var connection = {
    type: "",
    host: "",
    username: "",
    password: ""
};

var params = {
    DEV: "eth0",
    param1: "xxx",
    param2: "zzz"
};

var device = {
    MAC: ""
};

var util = new run_util_1.RunUtil({ params: params, device: device }, connection);

(async () => { 
    try {
        await script();    
    } catch (error) {
        console.log(error);
    }
    
    console.log("Params Out:", params);
    console.log("Log:", util.l_log);
})();

async function script() {

    let radius = await util.getRadius();
    params.users  = await radius.query('SELECT * FROM Users', []);

    util.log("xxx");
}
